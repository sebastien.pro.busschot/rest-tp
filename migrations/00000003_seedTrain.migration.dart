import 'dart:async';
import 'package:aqueduct/aqueduct.dart';   

class Migration3 extends Migration { 
  @override
  Future upgrade() async {
   
  }
  
  @override
  Future downgrade() async {}
  
  @override
  Future seed() async {

    final List<Map<String, dynamic>> trains = [
      {
        "villeDepart" : "Béziers",
        "villeArivee" : "Montpellier",
        "heureDepart" : 1930
      },
      {
        "villeDepart" : "Paris",
        "villeArivee" : "Montpellier",
        "heureDepart" : 1023
      },
      {
        "villeDepart" : "Montpellier",
        "villeArivee" : "Paris",
        "heureDepart" : 2258
      },
      {
        "villeDepart" : "Lyon",
        "villeArivee" : "Bordeaux",
        "heureDepart" : 1000
      },
      {
        "villeDepart" : "Agde",
        "villeArivee" : "Moscou",
        "heureDepart" : 2358
      },
      {
        "villeDepart" : "St Tibère",
        "villeArivee" : "Montépython",
        "heureDepart" : 1620
      },
      {
        "villeDepart" : "Béziers",
        "villeArivee" : "Béziers",
        "heureDepart" : 1212
      }
    ];

    for(final Map<String, dynamic> train in trains) {
      await database.store.execute(
          "INSERT INTO _Train (villeDepart, villeArrivee, heureDepart) VALUES (@villeDepart, @villeArrivee, @heureDepart)",
          substitutionValues: {
            "villeDepart": train["villeDepart"],
            "villeArrivee": train["villeArivee"],
            "heureDepart": train["heureDepart"]
          }
      );
    }

  }
}
