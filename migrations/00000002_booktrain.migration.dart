import 'dart:async';
import 'package:aqueduct/aqueduct.dart';

class Migration2 extends Migration { 
  @override
  Future upgrade() async {
    database.createTable(SchemaTable("_BookTrain", [SchemaColumn("bookNumber", ManagedPropertyType.integer, isPrimaryKey: true, autoincrement: true, isIndexed: false, isNullable: false, isUnique: false),SchemaColumn("numberOfPLaces", ManagedPropertyType.integer, isPrimaryKey: false, autoincrement: false, isIndexed: false, isNullable: false, isUnique: false)]));
		database.addColumn("_BookTrain", SchemaColumn.relationship("train", ManagedPropertyType.integer, relatedTableName: "_Train", relatedColumnName: "numTrain", rule: DeleteRule.nullify, isNullable: true, isUnique: true));
  }
  
  @override
  Future downgrade() async {}
  
  @override
  Future seed() async {}
}
