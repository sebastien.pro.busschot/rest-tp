# rest_tp
[Lien vers la documentation Postman](https://documenter.getpostman.com/view/11378334/TW73F6Ae)
## Important
Version de dart 2.7.2

Version de POSTGRE 13.1

Version Aqueduct 3.3.0+1

### Installer sous ubuntu
```bash
# se connecter avec l'utilisateur possres
sudo -i -u postgres 
Password: 

# lancer psql
psql
```

Créer la BDD dans postgres :

```sql
CREATE DATABASE rest;
CREATE USER urest WITH PASSWORD 'passwordrest';
GRANT ALL ON DATABASE rest TO urest;
```

Dans le dossier du projet:
```bash
pub get
aqueduct db upgrade
aqueduct serve
```

## Running the Application Locally

Run `aqueduct serve` from this directory to run the application. For running within an IDE, run `bin/main.dart`. By default, a configuration file named `config.yaml` will be used.

To generate a SwaggerUI client, run `aqueduct document client`.

## Running Application Tests

To run all tests for this application, run the following in this directory:

```
pub run test
```

The default configuration file used when testing is `config.src.yaml`. This file should be checked into version control. It also the template for configuration files used in deployment.

## Deploying an Application

See the documentation for [Deployment](https://aqueduct.io/docs/deploy/).
