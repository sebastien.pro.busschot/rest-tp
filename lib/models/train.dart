import 'package:rest_tp/models/bookTrain.dart';
import 'package:rest_tp/rest_tp.dart';

class Train extends ManagedObject<_Train> implements _Train {}

class _Train {
  @Column(primaryKey: true, autoincrement: true)
  int numTrain;

  @Column()
  String villeDepart;

  @Column()
  String villeArrivee;

  @Column()
  int heureDepart;

  BookTrain bookTrain;
}
