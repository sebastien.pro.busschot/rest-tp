import 'package:rest_tp/models/train.dart';
import 'package:rest_tp/rest_tp.dart';

class BookTrain extends ManagedObject<_BookTrain> implements _BookTrain {}

class _BookTrain {
  @Column(primaryKey: true, autoincrement: true)
  int bookNumber;

  @Column()
  int numberOfPLaces;

  @Column()
  int sales;

  @Relate(#bookTrain)
  Train train;

}
