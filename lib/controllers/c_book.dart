import 'package:rest_tp/models/bookTrain.dart';
import 'package:rest_tp/rest_tp.dart';

class CBookTrain extends ResourceController {
  CBookTrain(this.context);

  final ManagedContext context;

  @Operation.post()
  Future<Response> create(@Bind.body() BookTrain bookTrain) async {
    final result = await CBookTrain.insert(context, bookTrain);
    return Response.ok(result);
  }

  @Operation.delete('id')
  Future<Response> delete(@Bind.path('id') int id) async {
    final query = Query<BookTrain>(context)
      ..where((model) => model.bookNumber).equalTo(id);

    final int deleted = await query.delete();

    if(deleted == 0){
      return Response.noContent();
    }

    return Response.ok({
      "status" : 200,
      "deleted" : true
    });
  }

  @Operation.put('id')
  Future<Response> update(@Bind.path('id') int id, @Bind.body() BookTrain bookTrain) async {
    final query = Query<BookTrain>(context)
      ..values.sales = bookTrain.sales
      ..values.numberOfPLaces = bookTrain.numberOfPLaces
      ..where((model) => model.bookNumber).equalTo(id);

    final updated = await query.updateOne();

    if(updated == null){
      return Response.noContent();
    }

    return Response.ok(updated);
  }

  @Operation.get('id')
  Future<Response> retrieve(@Bind.path('id') int id) async {
    final query = Query<BookTrain>(context)
      ..join(object: (b) => b.train)
      ..where((model)=>model.bookNumber).equalTo(id);

    final subject = await query.fetchOne();

    if(subject == null){
      return Response.noContent();
    }

    return Response.ok(subject);
  }

  @Operation.get()
  Future<Response> getAll() async {
    final query = Query<BookTrain>(context)
      ..join(object: (b) => b.train);
    final List<BookTrain> rows = await query.fetch();

    return Response.ok(rows);
  }

  static Future<dynamic> insert(ManagedContext context, BookTrain bookTrain) async {
    final query = Query<BookTrain>(context)
      ..values = bookTrain;

    return await query.insert();
  }

  static Future<int> deleteInCascade(int idTrain, ManagedContext context) async {
    final query = Query<BookTrain>(context)
      ..where((model) => model.train.numTrain).equalTo(idTrain);

    return await query.delete();
  }
}
