import 'package:rest_tp/models/bookTrain.dart';
import 'package:rest_tp/rest_tp.dart';

class CSales extends ResourceController {
  ManagedContext context;

  CSales(this.context);

  @Operation.put("idBook")
  Future<Response> buy(@Bind.path("idBook") int idBook) async {
    return await saleAndCancel(idBook, true);
  }

  @Operation.delete("idBook")
  Future<Response> cancel(@Bind.path("idBook") int idBook) async {
    return await saleAndCancel(idBook, false);
  }

  Future<Response> saleAndCancel(int idBook, bool buy) async {
    final Query<BookTrain> query = Query<BookTrain>(context)
        ..where((b) => b.bookNumber).equalTo(idBook);

    final BookTrain book = await query.fetchOne();

    if(book == null){
      return Response.noContent();
    }

    final bool outOfSale = book.numberOfPLaces - book.sales <= 0 && buy;
    final bool outOfSold = book.numberOfPLaces - book.sales >= book.numberOfPLaces && !buy;

    if(outOfSold || outOfSale){
      return Response.badRequest();
    }

    int balance = buy ? 1 : -1;

    query.values.sales = book.sales + balance;

    final BookTrain ubook = await query.updateOne();

    return Response.ok(ubook);
  }

}
