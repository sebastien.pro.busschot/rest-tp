import 'package:rest_tp/models/bookTrain.dart';
import 'package:rest_tp/models/train.dart';
import 'package:rest_tp/rest_tp.dart';

class CQuery extends ResourceController {
  CQuery(this.context);

  ManagedContext context;

  @Operation.get("entity","col","value")
  Future<Response> getByQuery(@Bind.path("entity") String entity, @Bind.path("col") String col, @Bind.path("value") String value) async {

    dynamic datas;

    if(entity == "train"){
      const List<String> cols = ["numTrain","villeDepart","villeArrivee","heureDepart"];
      if(!cols.contains(col)){
        return Response.badRequest();
      }

      final Query<Train> query = Query<Train>(context)
        ..predicate = getPredicate(col, value);

      datas = await query.fetch();

    } else if (entity == "book"){

      const List<String> cols = ["bookNumber","numberOfPLaces","sales","heureDepart"];
      if(!cols.contains(col)){
        return Response.badRequest();
      }

      final Query<BookTrain> query = Query<BookTrain>(context)
        ..predicate = getPredicate(col, value);

      datas = await query.fetch();
    } else {
      return Response.badRequest();
    }

    return Response.ok(datas);
  }

  QueryPredicate getPredicate(String col, String value){
    return QueryPredicate("${col} = @value", {
      "value": value
    });
  }
}
