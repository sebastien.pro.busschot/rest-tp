import 'package:rest_tp/controllers/c_book.dart';
import 'package:rest_tp/models/bookTrain.dart';
import 'package:rest_tp/models/train.dart';
import 'package:rest_tp/rest_tp.dart';

class CTrain extends ResourceController {
  CTrain(this.context);

  final ManagedContext context;

  @Operation.post()
  Future<Response> create(@Bind.body() Train train) async {
    final query = Query<Train>(context)
      ..values = train;

    final Train result = await query.insert();

    final BookTrain newBook = BookTrain();
    newBook.numberOfPLaces = 0;
    newBook.sales = 0;
    newBook.train = Train();
    newBook.train.numTrain = result.numTrain;

    await CBookTrain.insert(context, newBook);

    return Response.ok(result);
  }

  @Operation.delete('id')
  Future<Response> delete(@Bind.path('id') int id) async {
    final query = Query<Train>(context)
      ..where((model) => model.numTrain).equalTo(id);

    final int bDeleted = await CBookTrain.deleteInCascade(id, context);

    final int deleted = await query.delete();

    if(deleted == 0){
      return Response.noContent();
    }

    return Response.ok({
      "status" : 200,
      "train deleted" : true,
      "book deleted" : bDeleted > 0
    });
  }

  @Operation.put('id')
  Future<Response> update(@Bind.path('id') int id, @Bind.body() Train train) async {
    final query = Query<Train>(context)
      ..values.heureDepart = train.heureDepart
      ..values.villeDepart = train.villeDepart
      ..values.villeArrivee = train.villeArrivee
      ..where((model) => model.numTrain).equalTo(id);

    final updated = await query.updateOne();

    if(updated == null){
      return Response.noContent();
    }

    return Response.ok(updated);
  }

  @Operation.get('id')
  Future<Response> retrieve(@Bind.path('id') int id) async {
    final query = Query<Train>(context)
      ..join(object: (t) => t.bookTrain)
      ..where((model)=>model.numTrain).equalTo(id);

    final subject = await query.fetchOne();

    if(subject == null){
      return Response.noContent();
    }

    return Response.ok(subject);
  }

  @Operation.get()
  Future<Response> getAll() async {
    final query = Query<Train>(context)
      ..join(object: (t) => t.bookTrain);
    final List<Train> trains = await query.fetch();

    return Response.ok(trains);
  }
}
