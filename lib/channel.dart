import 'package:rest_tp/controllers/c_book.dart';
import 'package:rest_tp/controllers/c_train.dart';

import 'controllers/c_query.dart';
import 'controllers/c_sales.dart';
import 'rest_tp.dart';

class ServConfig extends Configuration {
  ServConfig(String path) : super.fromFile(File(path));

  DatabaseConfiguration database;
}

class RestTpChannel extends ApplicationChannel {

  ManagedContext context;
  ServConfig config;

  @override
  Future prepare() async {
    logger.onRecord.listen((rec) => print("$rec ${rec.error ?? ""} ${rec.stackTrace ?? ""}"));

    config = ServConfig(options.configurationFilePath);
    final dataModel = ManagedDataModel.fromCurrentMirrorSystem();
    final psc = PostgreSQLPersistentStore.fromConnectionInfo(
        config.database.username,
        config.database.password,
        config.database.host,
        config.database.port,
        config.database.databaseName);

    context = ManagedContext(dataModel, psc);

  }

  @override
  Controller get entryPoint {
    final router = Router();

    router
      .route("/train/[:id]")
      .link(() => CTrain(context));

    router
        .route("/book/[:id]")
        .link(() => CBookTrain(context));

    router
        .route("/search/:entity/field/:col/value/:value")
        .link(() => CQuery(context));

    router
        .route("/sales/:idBook")
        .link(() => CSales(context));

    return router;
  }
}
